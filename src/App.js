import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {

  const [search, setSearch] = useState("")
  const [films, setFilms] = useState([])
  async function handleChangeSearch(event) {
    setSearch(event.target.value)
    var response = await axios.get("http://www.omdbapi.com/",
        {
          params:{
            apikey:"1c8dda37",
            s: event.target.value
          }
        }
        )/*.then(response => {
      console.log(response.data);
        if(response.data.Search){
          setFilms(response.data.Search)
        } else {
          setFilms([])
        }
    }).catch(reason => console.log("il y a eu une erreur"))
        .finally(()=> console.log())*/
    console.log("test")
  }


  function displayFilms() {
    return films.map(film => <div>
      <img src={film.Poster} alt=""/>
      <p>{film.Title}</p>
    </div>)
  }
  return (
    <div className="App">
      <header className="App-header">
        <input type="text" value={search} onChange={handleChangeSearch}/>
        {displayFilms()}
      </header>
    </div>
  );
}

export default App;
